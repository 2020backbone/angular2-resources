# Angular 2 #

## Agenda for weekly meetings ##
* Code analysis and discussion of home work
* Presentation on a new topic
* Task for next week

## Topics ##
- HTTP + RxJS - 03.12 (Gena)
- Templates, pipes, directives - 17.12 (Anton)
- Forms
- Dependency injection
- Advanced routing
- Testing

## Task for 03.12 ##
Create simple ng2 app which will contain 3 pages:
- List of blog posts (list view). Should also contain a link to Add post page
- One blog post (item view), which can be opened by clicking on blog post title in the list view
- Add blog post screen (title, post) without validation. 
Use router to navigate beetwen views. Store all data in local storage. Split every logical part into separate component 

# Resources #

Usefull Twitter list - https://twitter.com/gkr_tweet/lists/js

## TypeScript ##
- Official documentation - http://www.typescriptlang.org/Handbook
- Pluralsight course - https://app.pluralsight.com/library/courses/typescript/table-of-contents

## Intro video ##
- https://www.youtube.com/watch?v=HLvr4CHCots
- https://www.youtube.com/watch?v=LS3aewTkfHI
- https://www.youtube.com/watch?v=s0xootlbudI
- https://www.youtube.com/watch?v=KL4Yi3WtymA

## Video tutorials ##
- Egghead - https://egghead.io/technologies/angular2

## Angular 2 conferences ##
- Angular Connect - https://www.youtube.com/channel/UCzrskTiT_ObAk3xBkVxMz5g
- Angular U - https://www.youtube.com/watch?list=PLQrl6x_e_AZG0RdzIYVns4eGFxUEF61Lw&v=aHGmj_fqPLE

## Other resources ##
- https://github.com/AngularClass/awesome-angular2
- https://github.com/timjacobi/angular2-education